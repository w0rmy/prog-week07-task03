﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number");
            var number = int.Parse(Console.ReadLine());
            domath(number);
        }

        static void domath(int number)
        {
            Console.WriteLine($"{number} + {number} = {number + number}");
            Console.WriteLine($"{number} - {number} = {number - number}");
            Console.WriteLine($"{number} / {number} = {number / number}");
            Console.WriteLine($"{number} * {number} = {number * number}");
        }
    }
}
